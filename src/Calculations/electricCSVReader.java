/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculations;

import aboralidator.ElectricalJframe;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 *
 * @author Javier
 */
public class electricCSVReader {




    private static String [] time;
 
    private static  int[] day;
    private static  int[] month;
    private static  int[] year;
    private static  int[] hour;
    private static  int[] minute;


    private static String[] lineInstant;
    private static  String[] instant;
    private static  double[] current;
    
    private static  double[] Energy;
    
    private static int contador;
    
    
    
    
            public static void ReadCurrentCSV(String path) {  

        String strDateFormat = "yyyy-MM-dd hh: mm: ss"; // El formato de fecha está especificado  
        SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat); // La cadena de formato de fecha se pasa como un argumento al objeto 

        
        String line= "";
        String[] val = null;
        String [] time =null;
        String [] lineInstant=null;
        int i=0;
        

        try {
            BufferedReader br= new BufferedReader(new FileReader(path));
                line=br.readLine();
                
            while((line=br.readLine())!=null){

                val =line.split(",");

                time= (val[1].split("-"));
                
                lineInstant=time[2].split(" ");
                instant=lineInstant[1].split(":");
                hour[i]=Integer.parseInt(instant[0]);
                minute[i]=Integer.parseInt(instant[1]);
                //second[i]=Integer.parseInt(instant[2]);
                
                        day[i]= Integer.parseInt(lineInstant[0]);
                        month[i]=Integer.parseInt(time[1]);
                        year[i]=Integer.parseInt(time[0]);
                        
                        
                        if (val[2].contains("-") ){
                            current[i]=current[i-1];
                            
                        }else{
                            current[i]=Double.parseDouble(val[2]);
                        }
                        
                        
                        
               // validation.setFlow[i](Double.parseDouble(val[]);
                System.out.print(year[i]+"/");
                System.out.print(month[i]+"/");
                System.out.print(day[i]+"-");
                System.out.print(hour[i]+":");
                System.out.print(minute[i]+"------:");
                System.out.println(current[i]);
              
               
                i++;
       electricCSVReader.contador=i;
            }

            //validation.setT_in(Tin);
            //validation.setT_out(Tout);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }

    } 

     public static ElectricData [] ElectricDataCreator(double voltage){
         int nlineas=1;
         if(ElectricalJframe.trifasica.isSelected()){
             nlineas=3;
         }
            ElectricData [] monitorizadorE = new ElectricData [contador];
            int i=0;                 
            double energy=0;
            while(year[i]!=0){
                
                //filtro del ofset
                /*if(current[i]<0.16){
                    current[i]=0;
                }*/
                
                energy=nlineas*voltage*current[i]/1000;   
                monitorizadorE[i]=new ElectricData(energy,hour[i],year[i],month[i],minute[i],current[i],voltage,day[i]);
                i++;
            }
               
            return monitorizadorE;
        }

     public static ElectricData[] HourlyEnergy(ElectricData[] electricData){ //Calcula la energía en Wh
            int DataMinutes=5;
            int cuentaerrores=0;
            ElectricData [] hourlyMonitorizator = new ElectricData [100+contador/12];
            int j=0;
            double energy=0.0;
            for(int i=1;i<electricData.length;i++){
                if(electricData[i]!=null && electricData[i-1]!=null){

                if(electricData[i].getHour()==electricData[i-1].getHour()){
                    double energyaux=electricData[i-1].getEnergy()*DataMinutes*60/3600;//kWh

                    if (energyaux<0){
                        energyaux=0;
                    }
                    energy=energy+energyaux;                    
                }else if(electricData[i].getMinute()==0&&electricData[i-1].getMinute()==55){
                    double energyaux=electricData[i-1].getEnergy()*DataMinutes*60/3600;//kWh

                    if (energyaux<0){
                        energyaux=0;
                    }
                    energy=energy+energyaux;        

                    
                    
                hourlyMonitorizator [j]=new ElectricData(energy, electricData[i-1].getHour(), electricData[i-1].getYear(), electricData[i-1].getMonth(),electricData[i-1].getDay());
                System.out.println(
                electricData[i-1].getMonth()+"/"+electricData[i-1].getDay()+", hour:"+electricData[i-1].getHour()+ "; Energy:"+energy +"kWh");
                energy=0;
                j++;
                }
                }else{
                cuentaerrores++;
                System.out.println("Patata");
                }
                }

            System.out.println("errores:"+ cuentaerrores);
            return hourlyMonitorizator;  
        }
     
     
      public static ElectricData[] DaylyEnergy(ElectricData[] electricData){ //Calcula la energía en kWh

            ElectricData[] daylyMonitorizator = new ElectricData [10+contador/(12*24)];
            int j=0;
            int aux=0;
            double energy=0.0;
            for(int i=1;i<electricData.length;i++){
                if(electricData[i]!=null){
                    aux=i;
                    if(electricData[i].getDay()==electricData[i-1].getDay()){
                        energy=energy+electricData[i-1].getEnergy();//kWh

                    }else{
                        energy=energy+electricData[i-1].getEnergy();//kWh
                    daylyMonitorizator [j]=new ElectricData(energy, electricData[i-1].getHour(), electricData[i-1].getYear(), electricData[i-1].getMonth(),electricData[i-1].getDay());
                        System.out.println("month" + electricData[i-1].getMonth()+"day:"+(electricData[i-1].getDay())+ "Energy:"+energy +"kWh");
                    energy=0;
                    j++;
                    }
                    
                }
            }
            energy=energy+electricData[aux].getEnergy();//kWh
                    daylyMonitorizator [j]=new ElectricData(energy, electricData[aux].getHour(), electricData[aux].getYear(), electricData[aux].getMonth(),electricData[aux].getDay());
                        System.out.println("month" + electricData[aux].getMonth()+"day:"+(electricData[aux].getDay())+ "Energy:"+energy +"kWh");
            
          return daylyMonitorizator;  
        }
      
      
      public static AboraReader MonthtlyEnergy(ElectricData[] electricData){ //Calcula la energía en Wh
            int [] nMonthDays={31,28,31,30,31,30,31,31,30,31,30,31};
            double [] electricalEnergy={0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
            boolean [] monthComplete={false,false,false,false,false,false,false,false,false,false,false,false,false};
            double [] monthPercentage= new double[13];
            int dayAux=0;
            int monthAux=0;
            double energy=0.0;
            energy=energy+electricData[0].getEnergy();//kWh
                        monthAux=electricData[0].getMonth();
            int aux=0;
            for(int i=1;i<electricData.length;i++){
                
                if(electricData[i]!=null){
                 aux=i;
                 dayAux++;
                    if(electricData[i].getMonth()==electricData[i-1].getMonth()){
                        energy=energy+electricData[i-1].getEnergy();//kWh
                        
                    }else{
                        
                     energy=energy+electricData[i-1].getEnergy();
                     monthAux=electricData[i-1].getMonth()-1;
                    electricalEnergy [monthAux]=energy;
                        System.out.println("month: " + (monthAux+1)+", Energy:"+ energy +" kWh");
                    energy=0;
            if(nMonthDays[monthAux]<=dayAux){
                monthPercentage[monthAux]=100*dayAux/nMonthDays[monthAux];
                monthComplete[monthAux]=true;
            }else{
                monthPercentage[monthAux]=100*dayAux/nMonthDays[monthAux];
            }
                    dayAux=0;
                    

                    }
                } 
            }
            energy=energy+electricData[aux].getEnergy();
            monthAux=electricData[aux].getMonth()-1;
            electricalEnergy [monthAux]=energy;
            
            

                        System.out.println("month: " + (monthAux+1)+", Energy:"+ energy +" kWh");
            double totalEnergy=0;
            System.out.print("los meses con datos son: ");
            for(int i=0;i<electricalEnergy.length;i++){
                totalEnergy=totalEnergy+electricalEnergy[i];

                
                if (electricalEnergy[i]!=0){
                    System.out.print(i+1 + ", ");
                }
            }
            electricalEnergy[12]=totalEnergy;
            System.out.println("La energía total:" +totalEnergy+ "kWh");
            
            AboraReader monthlyEnergy=new AboraReader(monthComplete,monthPercentage,electricalEnergy);
            
          return monthlyEnergy;  
        }
     
     
     
    
        public electricCSVReader() {
        this.time = new String[2];
        this.lineInstant = new String[3];
        this.instant = new String[3];
        this.day=new int[105500];
        this.month=new int[105500];
        this.year=new int[105500];
        this.hour=new int[105500];
        this.minute=new int[105500];
        this.current = new double[105500];
        this.Energy = new double[105500];
    }

    public static String[] getTime() {
        return time;
    }

    public static void setTime(String[] time) {
        electricCSVReader.time = time;
    }



    public static int[] getDay() {
        return day;
    }

    public static void setDay(int[] day) {
        electricCSVReader.day = day;
    }

    public static int[] getMonth() {
        return month;
    }

    public static void setMonth(int[] month) {
        electricCSVReader.month = month;
    }

    public static int[] getYear() {
        return year;
    }

    public static void setYear(int[] year) {
        electricCSVReader.year = year;
    }

    public static int[] getHour() {
        return hour;
    }

    public static void setHour(int[] hour) {
        electricCSVReader.hour = hour;
    }

    public static int[] getMinute() {
        return minute;
    }

    public static void setMinute(int[] minute) {
        electricCSVReader.minute = minute;
    }

    public static String[] getLineInstant() {
        return lineInstant;
    }

    public static void setLineInstant(String[] lineInstant) {
        electricCSVReader.lineInstant = lineInstant;
    }

    public static String[] getInstant() {
        return instant;
    }

    public static void setInstant(String[] instant) {
        electricCSVReader.instant = instant;
    }

    public static double[] getFlow() {
        return current;
    }

    public static void setFlow(double[] flow) {
        electricCSVReader.current = flow;
    }

    public static double[] getEnergy() {
        return Energy;
    }

    public static void setEnergy(double[] Energy) {
        electricCSVReader.Energy = Energy;
    }

    public static int getContador() {
        return contador;
    }

    public static void setContador(int contador) {
        electricCSVReader.contador = contador;
    }
    
}
