/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculations;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Properties;

/**
 *
 * @author fran-
 */
 public class AboraReader {

    private static double [] energyT= new double[13];
    private static double [] energyE= new double[13];

    private static double [] energyTAboraS= new double[13];
    private static double [] energyEAboraS= new double[13];

    private static boolean [] monthCompleteT= new boolean [13];
    private static boolean [] monthCompleteE= new boolean [13];
    
    private static double [] monthPercentageT= new double[13];
    private static double [] monthPercentageE= new double[13];
    
    
    
    public static double[] getEnergyTAboraS() {
        return energyTAboraS;
    }

    public static void setEnergyTAboraS(double[] energyTAboraS) {
        AboraReader.energyTAboraS = energyTAboraS;
    }

    public static double[] getEnergyEAboraS() {
        return energyEAboraS;
    }

    public static void setEnergyEAboraS(double[] energyEAboraS) {
        AboraReader.energyEAboraS = energyEAboraS;
    }
    public static double[] getMonthPercentageT() {
        return monthPercentageT;
    }

    public static void setMonthPercentageT(double[] monthPercentageT) {
        AboraReader.monthPercentageT = monthPercentageT;
    }

    public static double[] getMonthPercentageE() {
        return monthPercentageE;
    }

    public static void setMonthPercentageE(double[] monthPercentageE) {
        AboraReader.monthPercentageE = monthPercentageE;
    }
    


    public static boolean[] getMonthCompleteT() {
        return monthCompleteT;
    }

    public static void setMonthCompleteT(boolean[] MonnthCompleteT) {
        AboraReader.monthCompleteT = MonnthCompleteT;
    }

    public static boolean[] getMonthCompleteE() {
        return monthCompleteE;
    }

    public static void setMonthCompleteE(boolean[] MonnthCompleteE) {
        AboraReader.monthCompleteE = MonnthCompleteE;
    }

    
  
        public AboraReader(){
            /*this.energyE=new double[13];
            this.energyT=new double[13];
            this.monthCompleteE=new boolean[13];
            this.monthCompleteT=new boolean[13];
            this.monthPercentageE=new double[13];
            this.monthPercentageT=new double[13];
*/
        
    }
    
    public AboraReader(double [] energyT,double [] energyE){
        this.energyT=energyT;
        this.energyE=energyE;
            this.monthCompleteE=null;
            this.monthCompleteT=null;
            this.monthPercentageE=null;
            this.monthPercentageT=null;
        
    }
    public AboraReader(double [] energyT,boolean [] monthCompleteT, double [] monthPercentageT){
        this.energyT=energyT;
        this.monthPercentageT=monthPercentageT;
        this.monthCompleteT=monthCompleteT;
            this.monthCompleteE=null;
            this.energyE=null;
            this.monthPercentageE=null;
    }           
            
            
       public AboraReader (boolean [] monthCompleteE, double [] monthPercentageE, double [] energyE){
        this.energyT=null;
        this.monthPercentageT=null;
        this.monthCompleteT=null;
            this.monthCompleteE=monthCompleteE;
            this.energyE=energyE;
            this.monthPercentageE=monthPercentageE;


    }
    
     public  double[] getEnergyT() {
        return energyT;
    }

    public static void setEnergyT(double[] energyT) {
        AboraReader.energyT = energyT;
    }

    public  double[] getEnergyE() {
        return energyE;
    }

    public static void setEnergyE(double[] energyE) {
        AboraReader.energyE = energyE;
    }

}
    

       
    
