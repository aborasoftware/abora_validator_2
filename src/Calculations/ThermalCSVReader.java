/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculations;

import Calculations.ThermalData;
import aboralidator.ThermalJFrame;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import javax.swing.JOptionPane;
 

public class ThermalCSVReader {

       private static String [] time;
        private static double[] Tin;
       private static double[] Tout;   
    private static  int[] dayT;
    private static  int[] monthT;
    private static  int[] yearT;
    private static  int[] hourT;
    private static  int[] minuteT;
    
    
    private static  int[] dayF;
    private static  int[] monthF;
    private static  int[] yearF;
    private static  int[] hourF;
    private static  int[] minuteF;

    private static String[] lineInstant;
    private static  String[] instant;
    private static  double[] flow;
    
    private static  int[] dayE;
    private static  int[] monthE;
    private static  int[] yearE;
    private static  int[] hourE;
    private static  int[] minuteE;
    private static  double[] tEnergy;
    
    private static int contador;
    
        public ThermalCSVReader() {
        this.Tin = new double[105500];
        this.Tout = new double[105500];
        this.flow = new double[105500];
        this.tEnergy = new double[105500];
        
        this.time = new String[2];
        this.lineInstant = new String[3];
        this.instant = new String[3];
        this.dayT=new int[105500];
        this.monthT=new int[105500];
        this.yearT=new int[105500];
        this.hourT=new int[105500];
        this.minuteT=new int[105500];
        
        this.dayF=new int[105500];
        this.monthF=new int[105500];
        this.yearF=new int[105500];
        this.hourF=new int[105500];
        this.minuteF=new int[105500];
        



                   
    }
    
    
    



            
 

    public static void ReadTempCSV(String path) {  

        String strDateFormat = "yyyy-MM-dd hh: mm: ss"; // El formato de fecha está especificado  
        SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat); // La cadena de formato de fecha se pasa como un argumento al objeto 

        
        

        
        String line= "";
        String[] val = null;
        String [] time =null;
        String [] lineInstant=null;
        int i=0;
        

        try {
            BufferedReader br= new BufferedReader(new FileReader(path));
                line=br.readLine();
                
            while((line=br.readLine())!=null){

                val =line.split(",");

                time= (val[1].split("-"));
                
                        lineInstant=time[2].split(" ");
                        instant=lineInstant[1].split(":");
                        hourT[i]=Integer.parseInt(instant[0]);
                        minuteT[i]=Integer.parseInt(instant[1]);
                //second[i]=Integer.parseInt(instant[2]);
                
                        dayT[i]= Integer.parseInt(lineInstant[0]);
                        monthT[i]=Integer.parseInt(time[1]);
                        yearT[i]=Integer.parseInt(time[0]);
                        
                        if (val[2].contains("-") || val[4].contains("-") ){
                            Tin[i]=Tin[i-1];
                            Tout[i]=Tout[i-1];
                        }else{
                            Tin[i]=Double.parseDouble(val[2]);
                            Tout[i]=Double.parseDouble(val[4]);
                        }

                        
               // validation.setFlow[i](Double.parseDouble(val[]);
                System.out.print(yearT[i]+"/");
                System.out.print(monthT[i]+"/");
                System.out.print(dayT[i]+"-");
                System.out.print(hourT[i]+":");
                System.out.println(minuteT[i]+"----");
                //System.out.println(second[i]+":");
               
                i++;
            }

            //validation.setT_in(Tin);
            //validation.setT_out(Tout);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }

    } 
        public static void ReadFlowCSV(String path) {  

        String strDateFormat = "yyyy-MM-dd hh: mm: ss"; // El formato de fecha está especificado  
        SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat); // La cadena de formato de fecha se pasa como un argumento al objeto 

        
        

        
        String line= "";
        String[] val = null;
        String [] time =null;
        String [] lineInstant=null;
        int i=0;
        

        try {
            BufferedReader br= new BufferedReader(new FileReader(path));
                line=br.readLine();
                
            while((line=br.readLine())!=null){

                val =line.split(",");

                time= (val[1].split("-"));
                
                lineInstant=time[2].split(" ");
                instant=lineInstant[1].split(":");
                hourF[i]=Integer.parseInt(instant[0]);
                minuteF[i]=Integer.parseInt(instant[1]);
                //second[i]=Integer.parseInt(instant[2]);
                
                        dayF[i]= Integer.parseInt(lineInstant[0]);
                        monthF[i]=Integer.parseInt(time[1]);
                        yearF[i]=Integer.parseInt(time[0]);
                        
                        
                        if (val[2].contains("-") ){
                            flow[i]=flow[i-1];
                            
                        }else{
                            flow[i]=Double.parseDouble(val[2]);
                        }
                        
                        
                        
               // validation.setFlow[i](Double.parseDouble(val[]);
                System.out.print(yearF[i]+"/");
                System.out.print(monthF[i]+"/");
                System.out.print(dayF[i]+"-");
                System.out.print(hourF[i]+":");
                System.out.print(minuteF[i]+"------:");
                System.out.println(flow[i]);
              
               
                i++;
       ThermalCSVReader.contador=i;
            }

            //validation.setT_in(Tin);
            //validation.setT_out(Tout);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }

    } 
        
        
        public static ThermalData[] HourlyEnergy(ThermalData[] ThermalData){ //Calcula la energía en Wh
            int DataMinutes=5;
            int cuentaerrores=0;
            ThermalData[] hourlyMonitorizator = new ThermalData [100+contador/12];
            int j=0;
            double energy=0.0;
            for(int i=1;i<ThermalData.length;i++){
                if(ThermalData[i]!=null && ThermalData[i-1]!=null){

                if(ThermalData[i].getHour()==ThermalData[i-1].getHour()){
                    double energyaux=ThermalData[i-1].getEnergy()*DataMinutes*60/3600;//kWh

                    if (energyaux<0){
                        energyaux=0;
                    }
                    energy=energy+energyaux;                    
                }else if(ThermalData[i].getMinute()==0&&ThermalData[i-1].getMinute()==55){
                    double energyaux=ThermalData[i-1].getEnergy()*DataMinutes*60/3600;//kWh

                    if (energyaux<0){
                        energyaux=0;
                    }
                    energy=energy+energyaux;        

                    
                    
                hourlyMonitorizator [j]=new ThermalData(energy, ThermalData[i-1].getHour(), ThermalData[i-1].getYear(), ThermalData[i-1].getMonth(),ThermalData[i-1].getDay());
                System.out.println(
                         ThermalData[i-1].getMonth()+"/"+ThermalData[i-1].getDay()+", hour:"+ThermalData[i-1].getHour()+ "; Energy:"+energy +"kWh");
                energy=0;
                j++;
                }
                }else{
                cuentaerrores++;
                System.out.println("Patata");
                }
                }

            System.out.println("errores:"+ cuentaerrores);
            return hourlyMonitorizator;  
        }
                public static ThermalData[] DaylyEnergy(ThermalData[] ThermalData){ //Calcula la energía en kWh

            ThermalData[] daylyMonitorizator = new ThermalData [10+contador/(12*24)];
            int j=0;
            int aux=0;
            double energy=0.0;
            for(int i=1;i<ThermalData.length;i++){
                if(ThermalData[i]!=null){
                    aux=i;
                    if(ThermalData[i].getDay()==ThermalData[i-1].getDay()){
                        energy=energy+ThermalData[i-1].getEnergy();//kWh

                    }else{
                        energy=energy+ThermalData[i-1].getEnergy();//kWh
                    daylyMonitorizator [j]=new ThermalData(energy, ThermalData[i-1].getHour(), ThermalData[i-1].getYear(), ThermalData[i-1].getMonth(),ThermalData[i-1].getDay());
                        System.out.println("month" + ThermalData[i-1].getMonth()+"day:"+(ThermalData[i-1].getDay())+ "Energy:"+energy +"kWh");
                    energy=0;
                    j++;
                    }
                    
                }
            }
            energy=energy+ThermalData[aux].getEnergy();//kWh
                    daylyMonitorizator [j]=new ThermalData(energy, ThermalData[aux].getHour(), ThermalData[aux].getYear(), ThermalData[aux].getMonth(),ThermalData[aux].getDay());
                        System.out.println("month" + ThermalData[aux].getMonth()+"day:"+(ThermalData[aux].getDay())+ "Energy:"+energy +"kWh");
            
          return daylyMonitorizator;  
        }
        
                
                
                
   public static AboraReader MonthtlyEnergy(ThermalData[] ThermalData){ //Calcula la energía en Wh
            int [] nMonthDays={31,28,31,30,31,30,31,31,30,31,30,31};
            double [] thermalEnergy={0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
            boolean [] monthComplete={false,false,false,false,false,false,false,false,false,false,false,false,false};
            double [] monthPercentage= new double[13];
            int day=0;
            int month=0;
            double energy=0.0;
            energy=energy+ThermalData[0].getEnergy();//kWh
                        month=ThermalData[0].getMonth();
            int aux=0;
            for(int i=1;i<ThermalData.length;i++){
                
                if(ThermalData[i]!=null){
                 aux=i;
                 day++;
                    if(ThermalData[i].getMonth()==ThermalData[i-1].getMonth()){
                        energy=energy+ThermalData[i-1].getEnergy();//kWh
                        
                    }else{
                        
                     energy=energy+ThermalData[i-1].getEnergy();
                     month=ThermalData[i-1].getMonth()-1;
                    thermalEnergy [month]=energy;
                        System.out.println("month: " + (month+1)+", Energy:"+ energy +" kWh");
                    energy=0;
            if(nMonthDays[month]<=day){
                monthPercentage[month]=100*day/nMonthDays[month];
                monthComplete[month]=true;
            }else{
                monthPercentage[month]=100*day/nMonthDays[month];
            }
                    day=0;
                    

                    }
                } 
            }
            energy=energy+ThermalData[aux].getEnergy();
            month=ThermalData[aux].getMonth()-1;
            thermalEnergy [month]=energy;
            
            

                        System.out.println("month: " + (month+1)+", Energy:"+ energy +" kWh");
            double totalEnergy=0;
            System.out.print("los meses con datos son: ");
            for(int i=0;i<thermalEnergy.length;i++){
                totalEnergy=totalEnergy+thermalEnergy[i];

                
                if (thermalEnergy[i]!=0){
                    System.out.print(i+1 + ", ");
                }
            }
            thermalEnergy[12]=totalEnergy;
            System.out.println("La energía total:" +totalEnergy+ "kWh");
            
            AboraReader monthlyEnergy=new AboraReader(thermalEnergy, monthComplete,monthPercentage);
            
          return monthlyEnergy;  
        }
   
   
   
        public static ThermalData [] ThermalDataCreator(double cp){
            ThermalData[] monitorizadorT = new ThermalData [contador];

            int i=0;
            int j=0;
            
            int cuentaCambio1=0;
            int cuentaCambio2=0;

          
                    
            double energy=0;
            
            while(yearT[i]!=0&& yearF[j]!=0 ){
            if(yearT[j]==yearF[i]){
                if(monthF[i]==monthT[j]){
                    if(dayF[i]==dayT[j]){
                        if (hourF[i]==hourT[j]){
                            if(minuteF[i]==minuteT[j]){
                                energy=cp*(flow[i]/60)*(Tout[j]-Tin[j]);
                                
                              monitorizadorT[i]=new ThermalData(energy,hourT[i],yearT[i],monthT[i],minuteT[i],flow[i],Tin[i],Tout[i],cp,dayT[i]);
        //                    monitorizador[i].setYear(yearT[i]);
    //                        monitorizador[i].setHour(hourT[i]);
    //                        monitorizador[i].setMinute(minuteT[i]);
    //                        monitorizador[i].setCp(cp);
    //                        monitorizador[i].setFlow(flow[i]);
    //                        monitorizador[i].setT_in(Tin[i]);
    //                        monitorizador[i].setT_out(Tout[i]);
    //                        monitorizador[i].setEnergy(cp*flow[i]*(Tout[i]-Tin[i]));

                                
                            }else if(minuteF[i]>minuteT[j]){
                                j++;
                                cuentaCambio1++;
                                System.out.println( minuteF[i]+"---diferente---"+minuteT[j]);
                            }else if(minuteF[i]<minuteT[j]){
                                i++;
                                cuentaCambio2++;
                                System.out.println( minuteF[i]+"---diferente---"+minuteT[j]);
                            }
                        }else if(hourF[i]>hourT[j]){
                                j++;
                                cuentaCambio1++;
                                System.out.println( minuteF[i]+"---diferente---"+minuteT[j]);
                            }else if(hourF[i]<hourT[j]){
                                i++;
                                cuentaCambio2++;
                                System.out.println( minuteF[i]+"---diferente---"+minuteT[j]);
                            }
                    }    
                }   
            }
            i++;
            j++;
        }
            CalculaCaudalMedio(flow);

                
            System.out.println("total desplazamientos i: "+cuentaCambio1);
            System.out.println("total desplazamientos i: "+cuentaCambio2);
            //System.out.println("Caudal medio "+caudalMedio);

          
            return monitorizadorT;
        }

//    static void WriteCSV(String filepath,ThermalData validation) {
//        
//        FileWriter fw;
//        try {
//            fw = new FileWriter(filepath);
//            BufferedWriter bw= new BufferedWriter(fw);
//            PrintWriter pw= new PrintWriter (bw);
//                        int i=0;
//                        pw.println("Q_tot (kW)");
//                        if (validation!=null){
//                        //double [] Q_tot=validation.getQ_total();
//                        
//            while(i<Q_tot.length){
//                        
//                        pw.println( String.valueOf(Q_tot[i]).replace(".",","));
//                        
//                        System.out.println(Q_tot[i]);
//                        i++;
//        }
//           
//            }
//                        pw.println( );
//                        pw.print("Cp elegido: ;");
//                        pw.print(validation.getCp()+";");
//                        pw.print("kW/(K*kg)");
//            
//            pw.println( );
//            pw.flush();
//            pw.close();
//            //JOptionPane.showConfirmDialog(null, "Guardado");
//        } catch (Exception e) {
//        }
//
//        
//        
//        
//    }
    
    public static void CalculaCaudalMedio (double [] caudal){
        int j=0;
            double [] auxCaudalMedio=new double[contador];
            double [] auxCaudalMedio_2=new double[contador];
            double caudalMedio=0;
            double caudalMedio_2=0;
            
            //Filtra los 0.0
            for(int i=0;i<caudal.length;i++){
                if(caudal[i]!=0.0){
                    auxCaudalMedio[j]=caudal[i];
                    caudalMedio=caudalMedio+auxCaudalMedio[j];
                    j++;
                }
            }
            caudalMedio=caudalMedio/(j+1);
           
            j=0;
            
            //filtra los puntos atípicos
            for(int i=0;i<auxCaudalMedio.length;i++){
                if(auxCaudalMedio[i]>caudalMedio*0.75 && auxCaudalMedio[i]<caudalMedio*1.25){
                    auxCaudalMedio_2[j]=auxCaudalMedio[i];
                    caudalMedio_2=caudalMedio_2+auxCaudalMedio_2[j];
                    j++;
                }
                
            }
           caudalMedio_2=60*caudalMedio_2/(j+1); 
           
           DecimalFormat df = new DecimalFormat("#.00");
        
           ThermalJFrame.caudalInstalacion.setText(df.format(caudalMedio_2));
           double caudOptimo=Double.parseDouble(ThermalJFrame.caudalOptimo.getText());
           double percentage = 100*caudalMedio_2/caudOptimo;
                   
           ThermalJFrame.jDiferencia.setText(df.format(percentage));
    }
    
}
        

