
package Calculations;

/**
 *
 * @author fran-
 */
public class ThermalData {
    
    
    private double energy;
    private int hour;
    private int year;
    private int minute,month,day;

    private double flow, T_in, T_out;
    private double cp;

    public ThermalData(){
        
    }
     public ThermalData(double energyT,int hourT, int yearT,int month, int day){
        
        energy=energyT;
        hour=hourT;
        year=yearT;
        this.month=month;
        this.day=day;


       
    }
            
    public ThermalData(double energyT,int hourT, int yearT,int month, int minuteT, double flowT, double T_inT,double T_outT, double cpT, int day){
        
        energy=energyT;
        hour=hourT;
        year=yearT;
        this.month=month;
        minute=minuteT;
        flow=flowT;
        T_in=T_inT;
        T_out=T_outT;
        cp=cpT;
        this.day=day;
       
        
    }
    
    public double getFlow() {
        return flow;
    }

    public void setFlow(double flow) {
        this.flow = flow;
    }

    public double getT_in() {
        return T_in;
    }

    public void setT_in(double T_in) {
        this.T_in = T_in;
    }

    public double getT_out() {
        return T_out;
    }

    public void setT_out(double T_out) {
        this.T_out = T_out;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public double getCp() {
        return cp;
    }

    public void setCp(double cp) {
        this.cp = cp;
    }
    
     public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }
    
    
      public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }
      public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
        
        
    }
    
    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
