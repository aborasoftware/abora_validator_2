
package Calculations;

/**
 *
 * @author fran-
 */
public class ElectricData {
    
    
    private double energy;
    private int hour;
    private int year;
    private int minute,month,day;

    private double current;
    private double voltage;

    public ElectricData(){
        
    }
     public ElectricData(double energy,int hour, int year,int month, int day){
        
        this.energy=energy;
        this.hour=hour;
        this.year=year;
        this.month=month;
        this.day=day;


       
    }
            
    public ElectricData(double energy,int hour, int year,int month, int minute, double current, double voltage, int day){
        
        this.energy=energy;
        this.hour=hour;
        this.year=year;
        this.month=month;
        this.minute=minute;
        this.current=current;
        this.voltage=voltage;
        this.day=day;
       
        
    }
    
    public double getFlow() {
        return current;
    }

    public void setFlow(double flow) {
        this.current = flow;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public double getCp() {
        return voltage;
    }

    public void setCp(double cp) {
        this.voltage = cp;
    }
    
     public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }
    
    
      public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }
      public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
        
        
    }
    
    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
